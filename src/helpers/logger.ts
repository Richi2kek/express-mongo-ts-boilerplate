class Logger {
    private reset = "\x1b[0m";
    private bright = "\x1b[1m";
    private dim = "\x1b[2m";
    private underscore = "\x1b[4m";
    private blink = "\x1b[5m";
    private reverse = "\x1b[7m";
    private hidden = "\x1b[8m";

    private fgBlack = "\x1b[30m";
    private fgRed = "\x1b[31m";
    private fgGreen = "\x1b[32m";
    private fgYellow = "\x1b[33m";
    private fgBlue = "\x1b[34m";
    private fgMagenta = "\x1b[35m";
    private fgCyan = "\x1b[36m";
    private fgWhite = "\x1b[37m";

    private bgBlack = "\x1b[40m";
    private bgRed = "\x1b[41m";
    private bgGreen = "\x1b[42m";
    private bgYellow = "\x1b[43m";
    private bgBlue = "\x1b[44m";
    private bgMagenta = "\x1b[45m";
    private bgCyan = "\x1b[46m";
    private bgWhite = "\x1b[47m";

    public info(info?: string): void {
        const msg =  info || '';
        console.log(this.fgGreen, "[INFO] " + msg);
        this.resetConsole();
    }

    public error(error?: string): void {
        const msg =  error || '';
        console.log(this.fgRed, "[ERROR] " + msg);
        this.resetConsole();
    }

    public post(post?: string): void {
        const msg =  post || '';
        console.log(this.fgBlue, "[POST] " + msg);
        this.resetConsole();
    }

    public get(get?: string): void {
        const msg =  get || '';
        console.log(this.fgYellow, "[GET] " + msg);
        this.resetConsole();
    }

    public put(put?: string): void {
        const msg =  put || '';
        console.log(this.fgCyan, "[UPDATE] " + msg);
        this.resetConsole();
    }

    public delete(del?: string): void {
        const msg =  del || '';
        console.log(this.fgMagenta, "[DELETE] " + msg);
        this.resetConsole();
    }

    private resetConsole(): void {
        console.log(this.reset);
    }
}

export const log: Logger = new Logger();

