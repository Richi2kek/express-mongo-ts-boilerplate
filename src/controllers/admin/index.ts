import { userControl } from './userControl';

export const adminController = {
    // User control
    getAllUsers: userControl.getAllUsers,
    getUserById: userControl.getUserById,
    getUserByUsername: userControl.getUserByUsername,
    getUserByEmail: userControl.getUserByEmail,
    addUser: userControl.addUser,
    updateUser: userControl.updateUser,
    deleteAllUsers: userControl.deleteAllUsers,
    deleteUserById: userControl.deleteUserById
};
