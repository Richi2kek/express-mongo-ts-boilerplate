import { Request, Response } from 'express';

import { log } from '../../../helpers/logger';
import { user } from '../../../models/user';
import { UserSchema } from '../../../interfaces/UserSchema';
import { UserData } from '../../../interfaces/UserData';

import { userConverter } from './userConverter';
import { usersConverter } from './usersConverter';
import { createNewUser } from './createNewUser';

const getAllUsers = (req: Request, res: Response) => {
    log.get('/api/v1/admin/user');
    user.getAll()
        .then((users: UserSchema[] ) => {
            usersConverter(users)
                .then(usersData => {
                    res.status(200)
                        .json({
                            success: true,
                            msg: 'Get all users',
                            data: usersData
                        });
                });
        })
        .catch((err: any) => {
            res.status(500)
                .json({err});
        });
};

const getUserById = (req: Request, res: Response) => {
    log.get(`/api/v1/admin/user/id/${req.params.id}`);
    user.getById(req.params.id)
        .then((userSchema: UserSchema) => {
            res.status(200)
                .json({userSchema});
        })
        .catch((err: any) => {
            res.status(500)
                .json({err});
        });
};

const getUserByUsername = (req: Request, res: Response) => {
    log.get(`/api/v1/admin/user/username/${req.params.username}`)
    user.getByUsername(req.user.username)
        .then((userSchema: UserSchema) => {
            res.status(200)
                .json({userSchema});
        })
        .catch((err: any) => {
            res.status(500)
                .json({err});
        });
};

const getUserByEmail = (req: Request, res: Response) => {
    log.get(`/api/v1/admin/user/email/${req.params.email}`);
    user.getByEmail(req.user.getByEmail)
        .then((userSchema: UserSchema) => {
            res.status(200)
                .json({userSchema});
        })
        .catch((err: any) => {
            res.status(500)
                .json({err});
        });
};

const addUser = (req: Request, res: Response) => {
    log.post('/api/v1/admin/user');
    log.post(JSON.stringify(req.body));
    createNewUser(req.body)
        .then((result: any) => {
            if (result.success) {
                res.status(200)
                    .json({
                        msg: result.msg,
                        data: result.data
                    });
            } else {
                res.status(401)
                    .json({msg: result.msg});
            }
        })
        .catch((err: any) => {
            res.status(500)
                .json({err});
        });
};

const updateUser = (req: Request, res: Response) => {
    log.put('/api/v1/admin/user');
    log.put(JSON.stringify(req.body));
    user.update(req.body)
        .then((result: any) => {
            res.status(200)
                .json({
                    success: true,
                    msg: 'User updated',
                });
        })
        .catch(err => { res.status(500).json(err); });
};


const deleteAllUsers = (req: Request, res: Response) => {
    log.delete(`/api/v1/admin/user/`);
    user.deleteAll()
        .then((isDeleted: boolean) => {
            if (isDeleted) {
                res.status(200)
                    .json({
                        success: isDeleted,
                        msg: 'All users deleted',
                    });
            } else {
                res.status(404)
                    .json({
                        success: isDeleted,
                        msg: 'No users found'
                    });
            }
        })
        .catch((err: any) => {
            res.status(500)
                .json({err});
        });
};

const deleteUserById = (req: Request, res: Response) => {
    log.delete(`/api/v1/admin/user/id/${req.params.id}`);
    user.deleteById(req.body._id)
        .then((isDeleted: boolean) => {
            if (isDeleted) {
                res.status(200)
                    .json({
                        success: isDeleted,
                        msg: 'User deleted by id'
                    });
            } else {
                res.status(404)
                    .json({
                        success: isDeleted,
                        msg: 'No user found with id'
                    });
            }
        });
};

export const userControl = {
    getAllUsers,
    getUserById,
    getUserByUsername,
    getUserByEmail,
    addUser,
    updateUser,
    deleteAllUsers,
    deleteUserById
};
