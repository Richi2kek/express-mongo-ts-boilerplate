import { UserSchema } from '../../../interfaces/UserSchema';

export const userConverter = (userSchema: UserSchema) => {
    return new Promise((resolve: any) => {
        const userData: any = {
            _id: userSchema._id || '',
            username: userSchema.username,
            email: userSchema.email,
            role: userSchema.role,
            friends: userSchema.friends
        };
        resolve(userData);
    });
};
