import { userConverter } from './userConverter';
import { UserSchema } from '../../../interfaces/UserSchema';

export const usersConverter = (users: UserSchema[]) => {
    return new Promise((resolve: any, reject: any) => {
        const usersData: any[] = [];
        users.forEach((userSchema: UserSchema) => {
            userConverter(userSchema)
                .then((userData: any) => {
                    usersData.push(userData);
                });
        });
        resolve(usersData);
    });
};
