import { user } from '../../models/user';

export const checkUserAuth = async(userData: any) => {
    try {
        const emailExist = await user.isEmailExist(userData.email);
        if (emailExist) {
            const newUser = await user.getByEmail(userData.email);
            const passwordIsSame = await user.IsPasswordSame(userData.password, newUser.password);
            if (passwordIsSame) {
                return {
                    success: true,
                    data: newUser
                };
            } else {
                return {
                    success: false,
                    msg: 'Incorrect password'
                };
            }
        } else {
            return {
                success: false,
                msg: 'Email does not exist'
            };
        }
    } catch (err) {
        return err;
    }
};
