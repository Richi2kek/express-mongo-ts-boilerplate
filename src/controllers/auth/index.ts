import { Request, Response, NextFunction } from 'express';
import * as jwt from 'jsonwebtoken';

import { createNewUser } from './createNewUser';
import { checkUserAuth } from './checkUserAuth';
import { createToken } from './createToken';
import { config } from '../../config/index';
import { UserSchema } from '../../interfaces/UserSchema';
import { log } from '../../helpers/logger';

const register = (req: Request, res: Response) => {
    log.post('/api/v1/auth/register ');
    log.post(JSON.stringify(req.body));
    createNewUser(req.body)
        .then((result: any) => {
            if (result.success) {
                res.status(200)
                    .json({
                        msg: result.msg,
                        data: result.data
                    });
            } else {
                res.status(401)
                    .json({msg: result.msg});
            }
        })
        .catch((err: any) => {
            res.status(500)
                .json({err});
        });
};

const login = (req: Request, res: Response) => {
    log.post('/api/v1/auth/login');
    log.post(JSON.stringify(req.body));
    checkUserAuth(req.body)
        .then((result: any) => {
            if (result.success) {
                const userSchema: UserSchema = result.data;
                createToken(userSchema)
                    .then((token) => {
                        res.header('Authorization', 'Bearer ' + token);
                        res.status(200)
                            .json({
                                success: true,
                                msg: 'User logged (check in header for token)'
                            });
                    })
                    .catch((err: any) => {
                        res.status(500)
                            .json(err);
                    });
            } else {
                res.status(401)
                    .json({msg: result.msg});
            }
        })
        .catch((err: any) => {
            res.status(500)
                .json({err});
        });
};

export const authController = {
    login,
    register,
};
