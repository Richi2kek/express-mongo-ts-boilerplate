import { user } from '../../models/user';

import { UserSchema } from '../../interfaces/UserSchema';

export const createToken = (userSchema: UserSchema) => {
    return new Promise((resolve: any, reject: any) => {
        user.generateToken(userSchema)
            .then((token: string) => {resolve(token); })
            .catch((err: any) => {reject(err); });
    });
};
