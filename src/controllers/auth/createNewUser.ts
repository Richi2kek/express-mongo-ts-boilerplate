import { user } from '../../models/user';

export const createNewUser = async(userData: any) => {
    try {
        const emailExist = await user.isEmailExist(userData.email);
        if (!emailExist) {
            const encryptedPassword: string = await user.encryptePassword(userData.password);
            userData.password = encryptedPassword;
            const newUser = await user.add(userData);
            return {
                success: true,
                msg: 'User created',
                data: newUser
            };
        } else {
            return {
                success: false,
                msg: 'Email already exist'
            };
        }
    } catch (err) {
        return err;
    }
};
