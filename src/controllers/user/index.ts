import { Request, Response } from 'express';

import { log } from '../../helpers/logger';
import { user } from '../../models/user';
import { UserSchema } from '../../interfaces/UserSchema';

const addFriend = (req: Request, res: Response) => {
    log.post('/api/v1/user/friend');
    log.post(JSON.stringify(req.body));
    const userId = req.params.id;
    const friendId = req.body.friendId;
    user.getById(userId)
        .then((userSchema: UserSchema) => {
            userSchema.friends.push(friendId);
            user.update(userSchema)
                .then((userUpdated: UserSchema) => {
                    res.status(200)
                        .json({
                            success: true,
                            msg: 'Friend added to user'
                        });
                })
                .catch((err: any) => {
                    res.status(500)
                        .json(err);
                });
        })
        .catch((err: any) => {
            res.status(500)
                .json(err);
        });
};


export const userController = {
    addFriend
};
