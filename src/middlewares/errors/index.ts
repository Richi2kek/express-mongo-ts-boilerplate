import { errorHandler } from './errorHandler';
import { notFound } from './notFound';

export const errors = { errorHandler, notFound };
