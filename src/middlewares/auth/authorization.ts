import { Request, Response, NextFunction } from 'express';

const adminCheck = (req: Request, res: Response, next: NextFunction) => {
    if (req.user.role !== 'admin') {
        res.status(402)
            .json({msg: 'Need admin right to access this ressource'});
    } else {
        next();
    }
};

export const authorization = {
    adminCheck,
};
