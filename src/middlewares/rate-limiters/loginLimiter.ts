import * as RateLimit from 'express-rate-limit';

export const loginLimiter = new RateLimit({
    windowMs: 60 * 1000, // 1 mn window
    delayAfter: 1, // begin slowing down responses after the first request
    delayMs: 3 * 1000, // slow down subsequent responses by 3 seconds per request
    max: 3, // start blocking after 5 requests
    message: "Too many tentative from this IP, please try again after one minute"
});
