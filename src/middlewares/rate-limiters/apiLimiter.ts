import * as RateLimit from 'express-rate-limit';

export const apiLimiter = new RateLimit({
    windowMs: 15 * 60 * 1000, // 15 minutes
    max: 100,
    delayMs: 0 // disabled
});
