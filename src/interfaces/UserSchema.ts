export interface UserSchema {
    _id?: string;
    username: string;
    email: string;
    password: string;
    role: string;
    friends: string[];
}
