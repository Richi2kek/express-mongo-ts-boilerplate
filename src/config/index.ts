export const config = {
    application: {
        title: 'My app',
        version: '0.1'
    },
    security: {
        corsOrigin: '*',
        userSecret: 'Sup3rC0ursD3N0d3JSAYn0v!',
        saltRound: 8
    },
    mongo: {
        host: 'localhost',
        database: 'default_db',
        port: '27017'
    }
};
