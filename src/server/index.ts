
import { app } from '../app';
import { log } from '../helpers/logger';

const port = normalizePort(process.env.PORT || '3000');
app.set('port', port);
app.listen(port, onListening);

function normalizePort(val: any) {
  const normalizedPort = parseInt(val, 10);
  if (isNaN(normalizedPort)) {
    // named pipe
    return val;
  }
  if (normalizedPort >= 0) {
    // port number
    return normalizedPort;
  }
  return false;
}

function onListening() {
  log.info(`server listening on http://localhost: ${port}`);
}
