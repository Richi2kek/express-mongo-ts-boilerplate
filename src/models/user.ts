import * as mongoose from 'mongoose';
import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';

import { UserSchema } from '../interfaces/UserSchema';
import { config } from '../config';

export class User {
    private static instance: User;
    schema: mongoose.Schema;
    model: mongoose.Model<any>;
    constructor() {
        this.schema = new mongoose.Schema({
            username: String,
            email: String,
            password: String,
            role: String,
            friends: [String]
        });
        this.model = mongoose.model('UserModel', this.schema);
    }

    public static get Instance() {
        return this.instance || (this.instance = new this());
    }

    public getAll(): Promise<UserSchema[]> {
        return new Promise((resolve: any, reject: any) => {
            this.model.find({}, (err: any, users: UserSchema[]) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(users);
                }
            });
        });
    }

    public getById(id: string): Promise<UserSchema> {
        return new Promise((resolve: any, reject: any) => {
            this.model.findOne({'_id': id}, (err: any, userSchema: UserSchema) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(userSchema);
                }
            });
        });
    }

    getManyById(ids: string[]): Promise<UserSchema[]> {
        return new Promise((resolve: any, reject: any) => {
            this.model.find({'_id': { $in: ids}}, (err: any, users: UserSchema[]) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(users);
                }
            });
        });
    }

    public getByUsername(email: string): Promise<UserSchema> {
        return new Promise((resolve: any, reject: any) => {
            this.model.findOne({'email': email}, (err: any, userSchema: UserSchema) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(userSchema);
                }
            });
        });
    }

    public getByEmail(email: string): Promise<UserSchema> {
        return new Promise((resolve: any, reject: any) => {
            this.model.findOne({'email': email}, (err: any, userSchema: UserSchema) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(userSchema);
                }
            });
        });
    }

    public add(userSchema: UserSchema): Promise<any> {
        return new Promise((resolve: any, reject: any) => {
            const newUser = new this.model(userSchema);
            newUser.save({}, (err: any, result: any) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
    }

    public update(userSchema: UserSchema): Promise<any> {
        return new Promise((resolve: any, reject: any) => {
            this.model.findOneAndUpdate({'_id': userSchema._id }, userSchema, (err: any, result: any) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
    }

    public deleteAll(): Promise<boolean> {
        return new Promise((resolve: any, reject: any) => {
            this.model.deleteMany({}, (err: any) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(true);
                }
            });
        });
    }

    public deleteById(id: string): Promise<boolean> {
        return new Promise((resolve: any, reject: any) => {
            this.model.deleteOne({'_id': id}, (err: any) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(true);
                }
            });
        });
    }

    public isUsernameValid(username: string): Promise<boolean> {
        return new Promise((resolve: any, reject: any) => {
            this.model.findOne({'username': username}, (err: any, userSchema: UserSchema) => {
                if (err) {
                    reject(err);
                } else {
                    if (userSchema) {
                        resolve(false);
                    } else {
                        resolve(true);
                    }
                }
            });
        });
    }

    public isEmailExist(email: string): Promise<boolean> {
        return new Promise((resolve: any, reject: any) => {
            this.model.findOne({'email': email}, (err: any, userSchema: UserSchema) => {
                if (err) {
                    reject(err);
                } else {
                    if (userSchema === null) {
                        resolve(false);
                    } else {
                        resolve(true);
                    }
                }
            });
        });
    }

    public encryptePassword(password: string): Promise<string> {
        return new Promise((resolve: any, reject: any) => {
            bcrypt.hash(password, config.security.saltRound, (err: any, encrypted: string) => {
                if (err) {
                    reject (err);
                } else {
                    resolve(encrypted);
                }
            });
        });
    }

    public IsPasswordSame(password: string, encrypted: string): Promise<boolean> {
        return new Promise((resolve: any, reject: any) => {
            bcrypt.compare(password, encrypted, (err: any, isSame: boolean) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(isSame);
                }
            });
        });
    }

    public generateToken(userSchema: UserSchema): Promise<string> {
        return new Promise((resolve: any, reject: any) => {
            jwt.sign(
                {
                    _id: userSchema._id,
                    username: userSchema.username,
                    email: userSchema.email,
                    role: userSchema.role
                },
                config.security.userSecret,
                {expiresIn: '12h'},
                (err: any, token: string) => {
                    if (err) {
                        reject({err: 'Error during token generation'});
                    } else {
                        resolve(token);
                    }
            });
        });
    }

    public decodeToken(token: string): Promise<any> {
        return new Promise((resolve: any) => {
            const decoded = jwt.decode(token, { complete: true });
            resolve(decoded);
        });
    }
}

export const user = User.Instance;
