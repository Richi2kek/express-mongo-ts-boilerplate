import { Router } from 'express';

import { adminForms } from '../../forms/admin';
import { adminController } from '../../controllers/admin/';

export const adminRouter: Router = Router();

// User
adminRouter.get('/user',
    adminController.getAllUsers
);

adminRouter.get('/user/id/:id',
    adminController.getUserById
);

adminRouter.get('/user/username/:username',
    adminController.getUserByUsername
);

adminRouter.get('/user/email/:email',
    adminController.getUserByEmail
);

adminRouter.post('/user',
    adminForms.addUserValidator,
    adminController.addUser
);

adminRouter.put('/user',
    adminForms.updateUserValidator,
    adminController.updateUser
);

adminRouter.delete('/user',
    adminController.deleteAllUsers
);

adminRouter.delete('/user/id/:id',
    adminController.deleteUserById
);



