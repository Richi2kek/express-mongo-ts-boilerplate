import { Request, Response, NextFunction, Router } from 'express';
import * as jwt from 'jsonwebtoken';

import { user } from '../models/user';
import { authRouter } from './auth';
import { adminRouter } from './admin';
import { userRouter } from './user';


export const indexRouter: Router = Router();

indexRouter.get('/', (req: Request, res: Response, next: NextFunction) => {
    if (req.header('Authorization')) {
        const token: string = req.header('Authorization') || '';
        user.decodeToken(token.split(' ')[1])
            .then((decoded: any) => {
                createInfos(decoded.payload)
                    .then((infos: any) => {
                        res.status(200)
                            .json(infos);
                    });
                });
    } else {
        res.status(200)
        .json({
            role: 'guest',
            links: {
                auth: [
                    {method: 'POST', rel: 'REGISTER', href: 'localhost:3000/api/v1/auth/register'},
                    {method: 'POST', rel: 'LOGIN', href: 'localhost:3000/api/v1/auth/login'}
                ]
            }
        });
    }

});

const createInfos = (payload: any) => {
    return new Promise((resolve: any, reject: any) => {
        switch (payload.role) {
            case 'user':
                const userInfos = {
                    role: 'user',
                    links: {
                        friends: [
                            {method: 'GET', rel: 'GET ALL FRIENDS', href: `localhost:3000/api/v1/user/friend/${payload._id}`},
                            {method: 'POST', rel: 'ADD FRIEND', href: `localhost:3000/api/v1/user/friend/${payload._id}`}
                        ]
                    }
                };
                resolve(userInfos);
                break;
            case 'admin':
                const adminInfos = {
                    role: 'admin',
                    links: {
                        user: [
                            {method: 'GET', rel: 'GET ALL USERS', href: 'localhost:3000/api/v1/admin/user/'},
                            {method: 'GET', rel: 'GET USER BY ID', href: 'localhost:3000/api/v1/admin/user/id/:id'},
                            {method: 'GET', rel: 'GET USER BY EMAIL', href: 'localhost:3000/api/v1/user/admin/email/:email'},
                            {method: 'GET', rel: 'GET USER BY USERNAME', href: 'localhost:3000/api/v1/admin/user/username/:username'},
                            {method: 'POST', rel: 'ADD USER', href: 'localhost:3000/api/v1/admin/user/'},
                            {method: 'PUT', rel: 'UPDATE USER BY ID', href: 'localhost:3000/api/v1/admin/user/:id'},
                            {method: 'DELETE', rel: 'DELETE ALL USERS', href: 'localhost:3000/api/v1/admin/user/delete/'},
                            {method: 'DELETE', rel: 'DELETE USER BY ID', href: 'localhost:3000/api/v1/admin/user/id/:id'},
                        ]
                    }
                };
                resolve(adminInfos);
                break;
            default:
                break;
        }
    });
};
