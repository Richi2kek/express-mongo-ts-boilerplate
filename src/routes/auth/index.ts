import { Express, Router, Request, Response, NextFunction } from 'express';

import { registerLimiter } from '../../middlewares/rate-limiters/registerLimiter';
import { loginLimiter } from '../../middlewares/rate-limiters/loginLimiter';
import { validatorHandler, authForms } from '../../forms';
import { authController } from '../../controllers/auth';


export const authRouter: Router = Router();

authRouter.post(
    '/register',
    registerLimiter,
    authForms.registerValidator,
    validatorHandler,
    authController.register
);

authRouter.post(
    '/login',
    loginLimiter,
    authForms.loginValidator,
    validatorHandler,
    authController.login,
);




