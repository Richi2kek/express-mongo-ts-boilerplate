import { Express, Router, Request, Response, NextFunction } from 'express';

import { userForms } from '../../forms/';

import { userController } from '../../controllers/user';
export const userRouter: Router = Router();

userRouter.post(
    '/friend/:id',
    userForms.friendValidator,
    userController.addFriend
);


