import * as mongoose from 'mongoose';

import { config } from '../config';
import { log } from '../helpers/logger';

class DBClient {
    private static instance: DBClient;
    db: mongoose.Connection;
    host: string;
    port: string;
    database: string;
    constructor() {
        this.host = config.mongo.host;
        this.port = config.mongo.port;
        this.database = config.mongo.database;
    }
    public static get Instance() {
        return this.instance || (this.instance = new this());
    }
    public connectToMongoDB() {
        mongoose.connect(`mongodb://${this.host}:${this.port}/${this.database}`, {
            useMongoClient: true
        });
         // Set mongoose Promise with 'new' javascript Promise
        (<any>mongoose).Promise = global.Promise;

        this.db = mongoose.connection;
        this.db.on('error', () => {
            log.error('Mongodb connection error!');
        });
        this.db.on('connected', () => {
            log.info('Mongodb successfully connected!');
        });
        process.on('SIGINT', () => {
            this.db.close(() => {
                log.info('Mongoose default connection disconnected!');
                process.exit(0);
            });
        });
    }
}

export const dbClient: DBClient = DBClient.Instance;
