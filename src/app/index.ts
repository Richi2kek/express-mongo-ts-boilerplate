
import * as express from 'express';
import * as favicon from 'serve-favicon';
import * as bodyParser from 'body-parser';
import * as jwt from 'express-jwt';
import * as cors from 'cors';
import * as helmet from 'helmet';
import * as path from 'path';

import { dbClient } from './DBClient';

import { indexRouter } from '../routes';
import { authRouter } from '../routes/auth';
import { adminRouter } from '../routes/admin';
import { userRouter } from '../routes/user';

import { authorization } from '../middlewares/auth/authorization';
import { apiLimiter } from '../middlewares/rate-limiters/apiLimiter';
import { errors } from '../middlewares/errors';
import { config } from '../config';
import { log } from '../helpers/logger';


log.info('Application starting...');
export const app: express.Application = express();
const auth = jwt({
    secret: config.security.userSecret
});
// DB
dbClient.connectToMongoDB();
// Middlewares
// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors({
    origin: config.security.corsOrigin,
    // headers ?
}));
app.use(helmet());

// Routes
app.use('/api/v1', indexRouter);
app.use('/api/v1/auth', authRouter);
app.use('/api/v1/admin/', auth, authorization.adminCheck, adminRouter);
app.use('/api/v1/user', auth, userRouter);
// Errors
app.use(errors.errorHandler);
app.use(errors.notFound);
