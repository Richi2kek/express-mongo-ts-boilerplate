import { validatorHandler } from './validatorHandler';

import { adminForms } from './admin';
import { userForms } from './user';
import { authForms } from './auth';

export {
    validatorHandler,
    adminForms,
    userForms,
    authForms
};
