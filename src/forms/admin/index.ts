import {
    addUserValidator,
    updateUserValidator
} from './userControl';

export const adminForms = {
    // User
    addUserValidator,
    updateUserValidator
};
