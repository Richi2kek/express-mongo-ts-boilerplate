import { body } from 'express-validator/check';

export const updateUserValidator = [
    body('_id', 'User id must be provided')
        .exists(),
    body('username', 'Username must be at least 6 chars')
        .exists()
        .isLength({min: 6}),
    body('email', 'Email must match with email pattern')
        .exists()
        .isEmail()
        .trim()
        .normalizeEmail(),
    body('role', 'Role must be provided')
        .exists()
];

