import { addUserValidator } from './addUserValidator';
import { updateUserValidator } from './updateUserValidator';

export {
    addUserValidator,
    updateUserValidator
};
