import { param } from 'express-validator/check';

export const idValidator = [
    param('id', 'Id must be provided in params')
];

