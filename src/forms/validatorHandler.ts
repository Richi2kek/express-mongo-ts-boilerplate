import { Request, Response, NextFunction } from 'express';
import { validationResult } from 'express-validator/check';
import { matchedData } from 'express-validator/filter';

export const validatorHandler = (req: any, res: Response, next: NextFunction) => {
    if (isDataValid(req)) {
        next();
    } else {
        res.status(402);
        res.json({
            msg: req._validationErrors[0].msg
        });
    }
};

const isDataValid = (req: any) => {
    if (req._validationErrors[0]) {
        return false;
    } else {
        return true;
    }
};
