import { body, param } from 'express-validator/check';

export const friendValidator = [
    body('friendId', 'Body must contains friend \'s ID')
        .exists(),

    param('id', 'Must send user id to param')
        .exists()
];

