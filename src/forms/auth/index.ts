import {loginValidator } from './loginValidator';
import { registerValidator } from './registerValidator';

export const authForms = {
    loginValidator,
    registerValidator,
};
