import { body } from 'express-validator/check';

export const loginValidator = [
    body('email', 'Email must match with email pattern')
        .exists()
        .isEmail()
        .trim()
        .normalizeEmail(),
    body('password', 'Password must be at least 6 chars and contains one number')
        .exists()
        .isLength({min: 6})
        .matches(/\d/)
];

